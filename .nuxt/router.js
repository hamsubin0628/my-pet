import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _cb801464 = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _4a5a1a69 = () => interopDefault(import('../pages/my-pet.vue' /* webpackChunkName: "pages/my-pet" */))
const _04a91f86 = () => interopDefault(import('../pages/test.vue' /* webpackChunkName: "pages/test" */))
const _008da88c = () => interopDefault(import('../pages/customer/form.vue' /* webpackChunkName: "pages/customer/form" */))
const _33b25294 = () => interopDefault(import('../pages/customer/list.vue' /* webpackChunkName: "pages/customer/list" */))
const _7b1c2008 = () => interopDefault(import('../pages/customer/list-filter-paging.vue' /* webpackChunkName: "pages/customer/list-filter-paging" */))
const _0388fa04 = () => interopDefault(import('../pages/my-menu/logout.vue' /* webpackChunkName: "pages/my-menu/logout" */))
const _4212db72 = () => interopDefault(import('../pages/customer/detail/_id.vue' /* webpackChunkName: "pages/customer/detail/_id" */))
const _7fedc7ab = () => interopDefault(import('../pages/customer/edit/_id.vue' /* webpackChunkName: "pages/customer/edit/_id" */))
const _0993e587 = () => interopDefault(import('../pages/vp/products/_subin.vue' /* webpackChunkName: "pages/vp/products/_subin" */))
const _5ecf5a92 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/login",
    component: _cb801464,
    name: "login"
  }, {
    path: "/my-pet",
    component: _4a5a1a69,
    name: "my-pet"
  }, {
    path: "/test",
    component: _04a91f86,
    name: "test"
  }, {
    path: "/customer/form",
    component: _008da88c,
    name: "customer-form"
  }, {
    path: "/customer/list",
    component: _33b25294,
    name: "customer-list"
  }, {
    path: "/customer/list-filter-paging",
    component: _7b1c2008,
    name: "customer-list-filter-paging"
  }, {
    path: "/my-menu/logout",
    component: _0388fa04,
    name: "my-menu-logout"
  }, {
    path: "/customer/detail/:id?",
    component: _4212db72,
    name: "customer-detail-id"
  }, {
    path: "/customer/edit/:id?",
    component: _7fedc7ab,
    name: "customer-edit-id"
  }, {
    path: "/vp/products/:subin?",
    component: _0993e587,
    name: "vp-products-subin"
  }, {
    path: "/",
    component: _5ecf5a92,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
