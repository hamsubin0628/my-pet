# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<CardPetInfo>` | `<card-pet-info>` (components/card-pet-info.vue)
- `<CommonLoadingIcon>` | `<common-loading-icon>` (components/common/loading-icon.vue)
- `<LayoutAdminHeader>` | `<layout-admin-header>` (components/layout/admin-header.vue)
- `<LayoutAdminSidebarAvatar>` | `<layout-admin-sidebar-avatar>` (components/layout/admin-sidebar-avatar.vue)
- `<LayoutAdminSidebarMenu>` | `<layout-admin-sidebar-menu>` (components/layout/admin-sidebar-menu.vue)
